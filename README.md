# SpringCloud集成OAuth2

## 要点
> 重点搭建OAuth2授权认证服务器、OAuth2资源服务器及OAuth2客户端服务器。  
> 其中OAuth2授权认证服务器需要重点了解自定义认证方式、自定义授权方式。  
> 其他重点内容后续再补充

* [oauth2源码](docs/OAuth2 code.md)
* [sso原理](docs/SSO.md)

## 参考
* [推荐文档](http://www.ruanyifeng.com/blog/2019/04/oauth_design.html "OAuth 2.0 的一个简单解释") [^1][^2]
* [参考文档](https://developer.aliyun.com/article/636281 "单点登录（SSO）看这一篇就够了")
* [参考文档](https://www.jianshu.com/p/a711ea5ad965 "一文读懂Spring Cloud Oauth2.0认证授权")
* [参考文档](https://blog.csdn.net/Little_fxc/article/details/92791883 "Spring Security Oauth2 如何增加自定义授权模式")
* [优秀源码](https://gitee.com/lvhaibao/spring-lhbauth "授权服务器")
* [优秀源码](https://github.com/zhangwei900808/awbeci-ssb "授权服务器")
* [优秀源码](https://blog.csdn.net/lixiang987654321/article/details/107745998 "资源服务器")
* [优秀源码](https://github.com/chengjiansheng/cjs-oauth2-example "客户端服务器")
* [源码-feign-demo](https://gitlab.com/tpc.edu/oauth2)

[^1]: [OAuth 2.0 的四种方式](http://www.ruanyifeng.com/blog/2019/04/oauth-grant-types.html)
[^2]: [GitHub OAuth 第三方登录示例教程](http://www.ruanyifeng.com/blog/2019/04/github-oauth.html)