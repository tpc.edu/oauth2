# 单点登录（SSO）

## 单点登录流程图

![SSO Flow Chart](notes/SSO Flow Chart.png "单点登录（SSO）流程图")

## 参考
* [单点登录（SSO）看这一篇就够了](https://developer.aliyun.com/article/636281)
* [SSO单点登录的研究](https://www.cnblogs.com/mxmbk/p/5675873.html)