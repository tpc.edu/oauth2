[TOC]

# Spring Security OAuth2 源码分析

## 要点
Spring Security OAuth2 主要有两部分功能：
1. **生成token**
2. **验证token**

对大概的流程进行了一次梳理

## Server端生成token （post /oauth/token）

![authorization](https://img2018.cnblogs.com/blog/733995/201811/733995-20181113143835947-1620376642.png "OAuth2授权源码")

`AuthorizationServerSecurityConfigurer`在配置阶段    如果允许`form`表单提交则会经过`ClientCredentialsTokenEndpointFilter`， 剩下的流程可以总结为生成token、存储token。
1. **生成token：按类型生成token，类型与oauth2四种授权类型（授权码、隐藏式、密码式、凭证式）相对应**
2. **存储token: 支持 内存、redis、数据库、等多种方式**

## Resource端验证token (访问受限接口）

![resource](https://img2018.cnblogs.com/blog/733995/201811/733995-20181113144929505-10694931.png "OAuth2资源源码")

`ResourceSecurityConfigurer`在配置阶段，对`Resource`进行配置，OAuth2核心过滤器 `OAuth2AuthenticationProcessingFilter`，如果请求中存在token 则进行验证，如果不存在则不验证（但是Spring Security会对接口权限进行验证)。

**存在token流程**：

1. 根据配置的`userInfoEndpointUrl`到`server`获取token的授权信息`OAuthAuthentication`。 
2. 根据返回结果判断token是否有效（**如果返回结果中存在error表明访问限制： 也就是说server对无效的token进行处理后，一定要返回非200的http响应Resource才能认定无效**）

**本文转载自**[博客园](https://www.cnblogs.com/)作者**[浮生半日](https://www.cnblogs.com/mxmbk/)**的[Spring Security OAuth2 源码分析](https://www.cnblogs.com/mxmbk/p/9952298.html)一文。