03. Endpoints自定义(修改错误响应信息/全局错误页配置) (HttpSecurity.accessDeniedPage("/login?authorization_error=true"))
04. 数据库表性能优化/刷新令牌/验证令牌/ip拦截/OA整合
-- ++++++++++++++++++++++++++++++++ 资源 ++++++++++++++++++++++++++++++++
@Override
public void configure(ResourceServerSecurityConfigurer resources) {
    resources.resourceId(Constants.RESOURCE_ID).stateless(true);
}
无状态: stateless

如果token是在授权头（Authorization header）中发送的，那么跨源资源共享(CORS)将不会成为问题，因为它不使用cookie。[请求头 Authorization: Bearer <token>]
token:  HTTP协议是无状态的，也就是说，如果我们已经认证了一个用户，那么他下一次请求的时候，服务器不知道我是谁，我们必须再次认证
Cookie: 传统的做法是将已经认证过的用户信息存储在服务器上，比如Session。用户下次请求的时候带着Session ID，然后服务器以此检查用户是否认证过。
// 如果关闭 stateless，则 accessToken 使用时的 session id 会被记录，后续请求不携带 accessToken 也可以正常响应
// stateless(false: 关闭无状态http协议,使用Cookie; true: 开启无状态http协议,使用 Authorization: Bearer <token>)
-- ++++++++++++++++++++++++++++++++ JWT ++++++++++++++++++++++++++++++++
JWT 公钥/私钥
keytool -genkeypair -alias mytest -keyalg RSA -keypass jwtkey -keystore mytest.jks -storepass jwtkey
keytool -list -rfc --keystore mytest.jks | openssl x509 -inform pem -pubkey
-- ++++++++++++++++++++++++++++++++ 获取用户IP ++++++++++++++++++++++++++++++++	
String requestIp = SpringContextUtils.getRequestIp(request);
-- ++++++++++++++++++++++++++++++++ 5个handler 1个filter 2个User(该方式感觉已过时) ++++++++++++++++++++++++++++++++	
01. AuthenticationEntryPoint 未登录访问时调用: 响应错误, 或者跳转至认证服务器登录;(未授权访问时调用)
02. AuthenticationSuccessHandler 登录成功时调用: 不做处理;(授权成功时调用)
03. AuthenticationFailureHandler 登录失败时调用: 响应错误, 或者默认处理;(授权失败时调用)
04. AccessDeniedHandler 已授权无权限时调用: 响应错误, 或者默认处理;(已授权访问无权访问的资源时调用)
05. LogoutSuccessHandler 登出成功时调用: 响应信息, 或者默认处理;(注销授权时调用)

06. OncePerRequestFilter 
解析请求头中的token, 将解析出的用户信息放至SpringSecurity的上下文中(SecurityContextHolder.getContext().setAuthentication(authentication);), 对于无法解析的直接放行;
后续 SpringSecurity 会去做 是否授权和权限校验

(★★★)这里需要注意，如果将SpringSecurity的session配置为无状态，或者不保存session，这里authentication为null！！(注意空指针问题)。（详情见下面的配置WebSecurityConfigurerAdapter）
//配置取消session管理,用Jwt来获取用户状态,否则即使token无效,也会有session信息,依旧判断用户为登录状态
-- ++++++++++++++++++++++++++++++++ 熟悉配置API ++++++++++++++++++++++++++++++++
01. 令牌失效默认值在 DefaultTokenServices 中修改, 令牌默认放在内存中, 重启会丢失, 所以需要放在存储介质中(将令牌放于Redis)
TokenStore 持久化token; InMemoryTokenStore(default)(内存存储); JdbcTokenStore(JDBC实现);
DefaultTokenServices token生成,过期,持久化等;
ClientDetailsService InMemoryClientDetialsService(default)(内存存储); JdbcClientDetailsService(JDBC实现);
client 数据库字段参考: BaseClientDetails

Endpoints自定义: SavedRequestAwareAuthenticationSuccessHandler
数据库配置 客户端信息/用户信息
JdbcClientDetail: JdbcClientDetailsService
JdbcUser: JdbcUserDetailsManager/JdbcDaoImpl.loadUserByUsername
Endpoints/Security配置:
ResourceServerSecurityConfigurer
AuthorizationServerSecurityConfigurer: checkTokenAccess; tokenKeyAccess; realm;
AuthorizationServerEndpointsConfigurer: tokenStore; accessTokenConverter; authenticationManager;

AuthenticationManager(权限管理者) -依赖-> AuthenticationProvider(权限供应者) -依赖-> UserDetailsService(权限服务商)
-- ++++++++++++++++++++++++++++++++ BUG ++++++++++++++++++++++++++++++++
BUG: OAuth2报Redis相关错误(序列化问题;找不到相关接口), 解决: spring-security-oauth2 高于 2.3.2.RELEASE
BUG: SpringBoot 2.x 中引入@EnableOAuth2Sso报错, 解决办法: 引入依赖 spring-security-oauth2-autoconfigure
BUG: SpringBoot1.5环境中, 端点/product/{id}, /order/{id}在认证授权之前均可正常访问: 由SpringBoot对过滤器链的调用顺序导致, 
     解决: 在app.properties中加入配置 security.oauth2.resource.filter-order = 3
-- ++++++++++++++++++++++++++++++++ Token ++++++++++++++++++++++++++++++++	
{
  "user_name": "zhangsan",
  "ADD_INFO": "zhangsan",
  "scope": [
    "user_info"
  ],
  "exp": 1571828054,
  "authorities": [
    "couponList",
    "memberExport"
  ],
  "jti": "71992a73-e739-4a2b-8324-bed3709f2c56",
  "client_id": "MemberSystem"
}
-- ++++++++++++++++++++++++++++++++ 登出 ++++++++++++++++++++++++++++++++
详见: TestEndpoints.java/RevokeTokenEndpoint.java
spring security实现注销功主要处理类是LogoutFilter,LogoutHandler,LogoutSuccessHandler
LogoutHandler注销方法
LogoutSuccessHandler主要定义了注销成功后的操作
login?logout: 如果我们不配置自定义登出配置，则spring默认的配置为：登出url：logout，就是我们直接调用此url即可实现注销